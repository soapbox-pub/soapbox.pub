---
title: Unlocking 50+ Mastodon Apps for Nostr with Ditto
excerpt: Thanks to Ditto, Nostr users can explore a decade's worth of applications built for decentralized software on the Fediverse.
coverImage: '/assets/blog/unlocking-mastodon-apps/unlocking-mastodon-apps.png'
date: '2024-06-19'
author:
  name: Mary Kate Fain
  picture: '/assets/avatars/mk.jpg'
ogImage:
  url: '/assets/blog/unlocking-mastodon-apps/unlocking-mastodon-apps.png'
---

## How Ditto is unlocking a decade's worth of decentralized projects for use on Nostr
**Before Nostr, there was the Fediverse...**

The Fediverse is a network of decentralized social media sites built on the shared ActivityPub protocol. The Fediverse has been around in some form since 2009, and there are over 20,000 different sites (“servers”) on the Fediverse, home to over 4.4 million users. 

Like Nostr, the Fediverse is decentralized and built on free software, however, there are major issues with the premise of ActivityPub for user freedom (primarily, the account portability). This is why Soapbox moved focus to Nostr in 2023. 

Mastodon is one of the most popular projects on the Fediverse, including both web and mobile apps. Hundreds of apps have been developed for Mastodon and the Fediverse in the past decade (some better than others). 

### Now, thanks to [Ditto](/ditto), Nostr users can post from Mastodon-compatible apps on Nostr!

The beauty of this is that Nostr can benefit from ten years of open source app developement for the decentralized web, rather than having to build everything from scratch. This opens up a world of possibility we're only just beginning to explore. For example, the web app Buffer allows users to post from multiple accounts on various platforms, analyze growth, and even use AI to generate highly engaging content. Now, Nostr users can log in and schedule posts on Buffer. This is just one example of potential utility for Mastodon/Nostr compatability provided by Ditto.

<figure>
  <img alt='Schedule posts in advance with Buffer' src='/assets/blog/unlocking-mastodon-apps/buffer2.png' />
  <figcaption>Schedule Notes in advance with Buffer</figcaption>
</figure>


## How does Ditto unlock Mastodon Apps for Ditto? 

Mastodon apps allow users to log in to any server*. Since Ditto implements the Mastodon API, it can work with most [Mastodon app](https://joinmastodon.org/apps). Simply type in the URL of any Ditto server, and then log in with any NIP-46 bunker (we recommend [nak](https://github.com/fiatjaf/nak)). 


```
~ go install github.com/fiatjaf/nak@latest
```
<br />

```
~ nak bunker --prompt-sec wss://ditto.pub/relay 
```

## Okay, but does this *really* work? 

That answer is more complicated. All of this works *in theory* when apps on both sides fully follow their various specs. For example, signing in with nak bunker (developed by Fiatjaf) to Mastodon's official apps works very well. However, many Mastodon apps are not correctly impelementing the Mastodon API or OAuth, and many Nostr bunkers do not work reliably for signing. Media uploads also do not currently work, although we hope to fix this soon. 

**We're still in the very early days of exploring the full power of this crossover. There is major opportunity for improvement in the Nostr ecosystem, and we hope to see it taken more seriously in the future. In the next release of Ditto, we plan to do more to fix what we can on our end, too.**

Below you can find some popular Mastodon apps, and their current known compatability status with Nostr via Ditto. [Browse all Mastodon Apps](https://joinmastodon.org/apps)


## Mastodon Apps for Android 

<figure>
  <div className='grid grid-cols-3 gap-2'>
    <img src='/assets/blog/unlocking-mastodon-apps/mastodon-android.png' />
    <img src='/assets/blog/unlocking-mastodon-apps/mastodon-app1.jpg' />
    <img src='/assets/blog/unlocking-mastodon-apps/mastodon-app2.jpg' />
  </div>
  <figcaption>Viewing Nostr through the official Mastodon Android app.</figcaption>
</figure>

- [Mastodon (official)](https://play.google.com/store/apps/details?id=org.joinmastodon.android) (*compatability: confirmed*)
- [Tusky](https://play.google.com/store/apps/details?id=com.keylesspalace.tusky) (*compatability: broken*)
- [Subway Tooter](https://play.google.com/store/apps/details?id=jp.juggler.subwaytooter) (*compatability: confirmed*)
- [Fedilab](https://play.google.com/store/apps/details?id=social.rodent) (*compatability: confirmed*)
- [Megalodon](https://play.google.com/store/apps/details?id=org.joinmastodon.android.sk) (*compatability: unconfirmed*)
- [Rodent](https://play.google.com/store/apps/details?id=social.rodent) (*compatability: broken*)


## Mastodon Apps for iOS

<figure>
  <div className='grid grid-cols-3 gap-2'>
    <img src='/assets/blog/unlocking-mastodon-apps/mastodon-ios1.jpeg' />
    <img src='/assets/blog/unlocking-mastodon-apps/mastodon-ios2.jpeg' />
    <img src='/assets/blog/unlocking-mastodon-apps/mastodon-ios3.jpeg' />
  </div>
  <figcaption>Viewing Nostr through the official Mastodon iOS app.</figcaption>
</figure>


- [Mastodon (official)](https://apps.apple.com/us/app/mastodon-for-iphone/id1571998974) (*compatability: confirmed*)
- [Mast](https://apps.apple.com/us/app/mast-for-mastodon/id1437429129) (*compatability: unconfirmed*)
- [Mammoth](https://apps.apple.com/us/app/mammoth-for-mastodon/id1667573899) (*compatability: broken*)
- [Ice Cubes](https://apps.apple.com/id/app/ice-cubes-for-mastodon/id6444915884) (*compatability: broken*)
- [Feather](https://apps.apple.com/us/app/feather-for-mastodon/id6446263061) (*compatability: broken*)




## Mastodon Apps for web

<figure>
  <img alt='Post to Nostr from Elk for Mastodon' src='/assets/blog/unlocking-mastodon-apps/elk.png' />
  <figcaption>Post to Nostr from Elk for Mastodon</figcaption>
</figure>

- [Buffer](https://elk.zone/) (*compatability: confirmed*)
- [Fedica](https://fedica.com/) (*compatability: confirmed*)
- [Elk](https://elk.zone/) (*compatability: confirmed*)
- [Mastodeck](https://mastodeck.com/) (*compatability: broken*)


---

**Note on censorship on Mastodon Apps**

*There is a history of censorship on some Mastodon apps (like Tusky), which have chosen to block users of particular servers or software from using their app. Since Nostr users can log in with any Ditto server URL, this will circumvent server-level blocks. However, some apps may still block Soapbox software, including Ditto.
