---
title: 'Nostr vs. Fediverse vs. Bluesky: A Comparison of Decentralized Social Protocols'
excerpt: While all three protocols aim to decentralize social media, they differ in their approach to decentralization, user identity control, and censorship resistance.
coverImage: '/assets/blog/protocols.png'
date: '2025-02-18'
author:
  name: M. K. Fain
  picture: '/assets/avatars/mk.jpg'
ogImage: 
  url: '/assets/blog/protocols.png'
---

### Comparing Decentralized Social Protocols: Fediverse (ActivityPub) vs. Bluesky (AtProto) vs. Nostr

The social web is rapidly evolving, and decentralized protocols are at the forefront of this shift. Instead of relying on single, centralized platforms like Twitter or Facebook, decentralized protocols allow users to connect across different apps, maintain control of their identities, and reduce reliance on corporate gatekeepers.

Three major protocols stand out in this space: **Fediverse (ActivityPub), Bluesky (AtProto), and Nostr**. While some of these decentrlaized platforms have become more popular than others, their different approaches to decentralization provide various strengths and weaknesses.

---

## About the Fediverse (ActivityPub)

### **Background & History**
The **Fediverse** is a collection of decentralized social networks powered by the **ActivityPub** protocol. The protocol was developed by the **W3C (World Wide Web Consortium)** and officially became a recommended standard in **2018**. It was designed to allow different social platforms to interoperate, meaning users on one platform can follow and interact with users on another.

ActivityPub grew in popularity due to concerns over centralized control of platforms like Twitter and Facebook. **Mastodon**, one of its most well-known applications, gained a massive influx of users in **2022** after Elon Musk acquired Twitter.

### **Popular Apps & Servers**
- **Mastodon** – The most popular microblogging platform in the Fediverse, often compared to Twitter.
- **PeerTube** – A decentralized alternative to YouTube.
- **Pixelfed** – A photo-sharing network similar to Instagram.
- **Pleroma** – A lightweight alternative to Mastodon.
- **Misskey** – A more feature-rich microblogging option.
- **Soapbox** – A frontend for ActivityPub servers.

### **How ActivityPub Works**
- The protocol allows **federation**, meaning different servers (instances) can communicate with each other.
- Users create an account on one server, but they can follow and interact with users on different servers.
- Each server is independently operated, so **admins have control over moderation and rules**.
- If an instance is shut down or a user is banned, the user will lose their entire account and data and must create a new account on another server manually.

---

## About Bluesky (AtProto)

### **Background & History**
Bluesky started as a Twitter-funded project in **2019**, with the goal of building a decentralized social networking protocol. The team, led by **Jay Graber**, launched **Authenticated Transfer Protocol (AtProto)** as the foundation of the network. In early **2023**, Bluesky opened up access to the public and positioned itself as an alternative to centralized social media.

### **Popular Apps & Servers**
- **Bluesky** – The official and only major client.
- While self-hosting is technically possible, the entire ecosystem revolves around **bsky.social**, making decentralization extremely limited.

### **How AtProto Works**
- Bluesky uses **DIDs (Decentralized Identifiers)** for accounts, but accounts are still managed centrally.
- AtProto allows users to **theoretically** move between different services. However, user keys are controlled by the server admin, and users' ability to move servers is dependent on the server's willingness to cooperate.
- Although AtProto is a decentralized protocol, in practice, **Bluesky is highly centralized** because the vast majority of users rely on **bsky.social**, which can block entire self-hosted servers.
- Self-hosting is highly restricted: servers are **limited to 100 users**, and **Bluesky can refuse to federate with them**.

---

## About Nostr

### **Background & History**
**Nostr (Notes and Other Stuff Transmitted by Relays)** was created by **fiatjaf** in **2020** as a censorship-resistant social protocol. Unlike ActivityPub and AtProto, Nostr is **not server-based** but instead operates through a **relay-based** model.

Nostr gained significant attention in **2023** after receiving endorsements from Bitcoin advocates and figures like Edward Snowden and Jack Dorsey. Learn more about Nostr in our [Nostr 101 guide](/blog/nostr101/).

### **Popular Apps & Clients**
- **Damus** – iOS client.
- **Amethyst** – Android client.
- **Ditto** – A web-based client.
- **Primal, Nos, Snort** – Other popular and growing clients.

### **How Nostr Works**
- **Relay-based rather than server-based**: Users publish messages to multiple relays, which are simple data broadcasters.
- **Decentralized identity**: Users have cryptographic key pairs (private/public keys) instead of usernames tied to servers.
- **Completely censorship-resistant**: Since identities are key-based, no central authority can delete or ban an account.
- **Moderation happens at the client and relay level**: If a relay blocks a user, they can still use another relay.

---

## Which Decentralized Protocol is the Best?

Each protocol has strengths and weaknesses, but when it comes to **true decentralization and censorship resistance**, **Nostr** is the best option.

### **Comparison Factors**
<figure style={{ width: '100%', height: 'auto' }}>
  <img alt='Comparison of Nostr, Fediverse, and Bluesky' src='/assets/blog/protocol-comparison-details.png' style={{ width: '100%', height: 'auto', objectFit: 'contain' }} />
</figure>

### **Nostr: The Only Truly Decentralized Protocol**
- **True decentralized identity** – Your Nostr account is tied to a cryptographic key, not a server.
- **Censorship resistance** – If a relay bans you, you can use another.
- **No gatekeepers** – Unlike Bluesky, where Bsky.social can block your server.
- **More flexible moderation** – Clients and relays, rather than entire accounts, can be moderated.

The downside? **Nostr is still young**, meaning there are fewer polished apps compared to the long history of ActivityPub. However, given its rapid growth, this is expected to improve over time.

---

## Follow Users Across Protocols with the Mostr Bridge

<iframe width='100%' height='400' src='https://www.youtube.com/embed/075wrIJ1vHg?si=xOhhKCyUgKuy_gdX' title='Nostr 101 Video' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowFullScreen></iframe>

The good news is that no matter which platform you chose, you don't need to be isolated from the others. The **[Mostr Bridge](https://mostr.pub/)** allows users to follow accounts across platforms.

The Mostr Bridge acts as a relay that translates posts between protocols, letting users follow eachother across multiple decentralized platforms. The bridge is **best experienced from the Nostr side**, by creating an account on Nostr and following your friends on the Fediverse and Bluesky from there.
