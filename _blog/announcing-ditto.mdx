---
title: Announcing Ditto
excerpt: "It has moderation. It has spam filters. It has NIP-05 self-service. It has quote posts, emoji reactions, and zaps. ⚡ The scope of this project is absolutely huge. It aims to take everything that's good about Mastodon and implement it on Nostr, with none of the downsides."
coverImage: '/assets/blog/announcing-ditto.jpeg'
date: '2024-06-14'
author:
  name: Alex Gleason
  picture: '/assets/avatars/alex.png'
ogImage:
  url: '/assets/blog/announcing-ditto.jpeg'
---

It's finally here. After a year of work, I'm pleased to announce the first release of Ditto! 🎉

**Ditto is a Nostr community server.** It has a built-in Nostr relay, a web UI, and it implements Mastodon's REST API.

It has moderation. It has spam filters. It has NIP-05 self-service. It has quote posts, emoji reactions, and zaps. ⚡ The scope of this project is absolutely huge. It aims to take everything that's good about Mastodon and implement it on Nostr, with none of the downsides.

A huge thank you to [OpenSats](/blog/soapbox-awarded-grant/) for sponsoring this project!

<iframe width='100%' height='400' src='https://www.youtube.com/embed/iZyqVGHjDGo?si=yJxMBV7ss2Ei7hQg' title='Nostr 101 Video' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowFullScreen></iframe>


## Soapbox on Nostr

One of the first things I wanted to do is make Soapbox work on Nostr. Soapbox is a polished UI for Mastodon that I started developing in 2019. It went on to become the web frontend for Truth Social in 2022. With Ditto, users of Nostr can use Donald Trump's preferred web interface to chat on Nostr &mdash; or, customize the design of the client to match your own vibe!

<figure>
  <img alt='Example Ditto Home Page' src='/assets/blog/curated-communties/ditto-server.png' />
  <figcaption>Try it out at <a href='https://ditto.pub/' target='_blank'>ditto.pub</a>!</figcaption>
</figure>

## 50+ Mastodon Apps

Since Ditto implements the Mastodon API, it can work with [any Mastodon app](https://joinmastodon.org/apps)!

<figure>
  <div className='grid grid-cols-2 gap-2'>
    <img src='/assets/blog/mastodon-android-ditto-1.png' />
    <img src='/assets/blog/mastodon-android-ditto-2.png' />
  </div>
  <figcaption>Viewing Nostr through the official Mastodon Android app.</figcaption>
</figure>

## Starting a Server

Ditto is designed to be easy for people to self-host. The goal is for people to build curated communities on many different domains.

Read [the docs](https://docs.soapbox.pub/ditto/install/) to set up your own server!

## Usernames

Ditto offers self-service of NIP-05 names on the domain.

Users can request a name, and then admins will have the opportunity to approve or deny the request. If approved, the user will be granted the name, and their posts will be promoted throughout the UI.

<figure>
  <img alt='Set your identity on Ditto' src='/assets/blog/curated-communties/set-identity.png' />
  <figcaption>Choose how to be identified on your home Ditto server</figcaption>
</figure>

## Domain Feeds

Ditto can display a feed of posts from any domain name. Check out [nostrplebs.com](https://ditto.pub/timeline/nostrplebs.com) or [fiatjaf.com](https://ditto.pub/timeline/fiatjaf.com) for examples.

<figure>
  <img alt='Example Ditto Local Feed' src='/assets/blog/curated-communties/local-feed.png' />
  <figcaption>Anyone with a NIP-05 on the server's domain shows in the Local Feed</figcaption>
</figure>

## Moderation

Each Ditto server can have a group of moderators and admins who manage the server. They can view reports, delete posts, and approve or reject users. Ditto offers the most robust moderation tools of any Nostr relay I'm aware of.

<figure>
  <img alt='Ditto trends' src='/assets/blog/announcing-ditto/reports.png' />
  <figcaption>Sample admin report queue on Ditto</figcaption>
</figure>

## Spam Filters

Ditto has a policy plugin system that allows people to write custom scripts in TypeScript to reject any event. Check out [Ditto Moderation Policies](https://docs.soapbox.pub/ditto/policies/) for more information!

## Trends

Ditto analyzes the events in your database and generates trends. It tracks trending hashtags, users, and posts to improve discoverability.

Since the data is public, any client can utilize it. For example, Coracle custom feeds have already [been implemented](https://njump.me/nevent1qqsxfxsslvxv8cn8rl4v56hff52fxkptnhwf4y2m6tfjppep9lff0agprpmhxue69uhhyetvv9ujumn0wdmksetjv5hxxmmdqgsf03c2gsmx5ef4c9zmxvlew04gdh7u94afnknp33qvv3c94kvwxgsklndqw) using Ditto trends!

<figure>
  <img alt='Ditto trends' src='/assets/blog/announcing-ditto/trends.png' />
  <figcaption>Trending posts and people on ditto.pub</figcaption>
</figure>

## Search

Ditto ships with full-text search enabled in Postgres, helping users quickly find people and posts. Users of Mastodon and Pleroma will be happy to see search functionality that actually works. 😂

<figure>
  <img alt='Searching "ditto" on Ditto' src='/assets/blog/ditto-search.png' />
  <figcaption>Searching "ditto" on Ditto</figcaption>
</figure>

## Zaps

Zaps, small Bitcoin transactions sent over the lightning network, have become central to the Nostr ecosystem. Ditto implements zaps on user profiles and posts. In future releases, we plan to add optional zaps to the selfservice NIP-05 username system, allowing Ditto admins to monetize their platforms. 

<figure>
  <img alt='Zap on Ditto' src='/assets/blog/announcing-ditto/zap2.png' />
  <figcaption>Zap on Ditto</figcaption>
</figure>

## Nostr Relay

Each Ditto server exposes a Nostr relay at `/relay`.

This is a fully-featured relay, with careful support for NIP-01 and much more, including NIP-50 search filters, deletions, COUNT's, and custom tag indexes.

## Media Attachments

Ditto supports various options for uploading media, including:

- nostr.build
- IPFS
- Blossom
- S3 (or any other S3-compatible service)

Check [the docs](https://docs.soapbox.pub/ditto/media/) for more information.

## Fully Customizable Themes

Ditto allows admins to feature help define their server's unique identity adding custom themes and logos. Define your entire color scheme through a single hex code, applicable seamlessly across both light and dark themes. Or, manually select each color for maximum personalization! 

<figure>
  <img alt='Custom theme editing on Ditto' src='/assets/images/ditto/theme-editor.png' />
  <figcaption>Custom theme editing on Ditto</figcaption>
</figure>

## Technical Overview

Ditto is built in Deno with TypeScript. It's basically a REST API with a built-in Nostr relay. This is the same design as Mastodon and Pleroma, replacing ActivityPub with Nostr.

Under the hood it uses Postgres. And it was built for and with [Nostrify](https://nostrify.dev/).

<figure>
  <img alt='Soapbox + Ditto' src='https://docs.soapbox.pub/assets/soapbox-overview.DsbCuGgP.svg' />
  <figcaption><a href='https://docs.soapbox.pub/ditto/' target='_blank'>Learn more</a></figcaption>
</figure>

## Signing Events

Users on Nostr control their own private keys, allowing them to sign into any client. Authorization in Ditto is done through NIP-46 remote signing, so users keys never directly touch Ditto.

Even Soapbox implements NIP-46 so it can act as the remote signer. Keys may be stored in the browser, or in a browser extension like Alby for the best security.

<figure>
  <img alt='NIP-46 signing' src='https://docs.soapbox.pub/assets/ditto-sign.dGTkeUGs.svg' />
  <figcaption>Ditto signs events with NIP-46. <a href='https://docs.soapbox.pub/ditto/signing' target='_blank'>Learn more.</a></figcaption>
</figure>

## Flagship Ditto Communities

The most important thing about Ditto is the ability for people to freely build, manage, and monetize their communities on Nostr. This is why the Fediverse has continued to thrive for over a decade despite the limitations of the protocol. We're lauching Ditto with three flagship servers to kickstart community development:

### ditto.pub

[Ditto.pub](https://ditto.pub) is the official flagship server of Ditto, running the stable latest version. It is open to anyone, with light moderation. We welcome curious Nostr and Fediverse users to reserve their free username on Ditto now!

### henhouse.social

[Henhouse.social](https://henhouse.social) is a women-only Ditto community opperated by M. K. Fain, owner of the largest feminist server on the Fediverse. This server will prioritize building community among women, while remaining connected to the larger Nostr community. 

### gleasonator.dev

[Gleasonator.dev](https://gleasonator.dev) is the primary development server for Soapbox and Ditto. Join it to test out all the latest features before they are launched to the public!

## Creator Program

The Ditto Creator Incubator empowers emerging creators by providing them with resources, support, and exposure to build and monetize their social community on Nostr. The program offers creators a unique opportunity to focus on their content without the constraints of traditional social media algorithms and censorship. Participants receive personalized onboarding, support for setting up Bitcoin micropayments with lighting, and promotion across multiple channels. 

[**Apply to the Ditto Creator Incubator →**](/creators/)

## What Next?

This early release includes everything that we think is important to make Ditto what it is. However, we know we still have a lot to improve and exciting ideas for the future! Here's our to do for the forseeable future:

### Improvements

- Bug and UX improvements
- Performance and stability
- Improve compatability with Mastodon apps
- Fix server downtime when relays restart 
- Improve Alby support for multiple accounts
- [View all known issues →](https://docs.soapbox.pub/ditto/known-issues)


### New Features

- Buy NIP-05 usernames with zaps
- Improve support for Mastodon apps
- Digital item shop for profile customization
- Direct Messages
- [Submit a feature request on Gitlab →](https://gitlab.com/soapbox-pub/ditto/-/issues)


## Mentorship

Supporting the growth of the open source development community is at the heart of our work at Soapbox. I am very proud that this past year has enabled me to provide mentorship to two young software developers, [Patrick dos Reis](https://njump.me/npub1gujeqakgt7fyp6zjggxhyy7ft623qtcaay5lkc8n8gkry4cvnrzqd3f67z) of Brazil and [shantaram](https://njump.me/npub1q469xmf77nt9ltu4ks3excgts36ayt6v99ryn4rv2r2axmm4ye7q3vnkqp) of India. Their contributions in the past few months have been immeasurable, and I could not be more grateful to have them on the Soapbox team.
  
*Want to get involved? [Learn more](/mentorships) about mentorships at Soapbox.*


## Special Thanks

I would like to give a special thanks to everyone who has made this past year possible: fiatjaf, for all your advice and support; the Soapbox community for being my early testers; and the generous people of the Nostr and Bitcoin community for allowing me to do this work. And, finally, a very special thanks to my wonderful, beautiful, kind, smart, and funny wife for all of her love, patience, and work to help my dreams come true. 😘