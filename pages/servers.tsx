import { IconExternalLink } from '@tabler/icons';

import Container from '../components/container';
import FormattedNumber from '../components/formatted-number';
import Layout from '../components/layout';
import PageTitle from '../components/page-title';
import TeamMember from '../components/team-member';
import UnifiedMeta from '../components/unified-meta';
import { fetchServers, Server } from '../lib/servers';

interface IServersPage {
  servers: Server[]
}

export default function ServersPage({ servers }: IServersPage) {
  return (
    <Layout>
      <UnifiedMeta
        title='Servers | Soapbox'
      />
      <Container>
        <div className='max-w-4xl mx-auto'>

        <PageTitle className='my-16'>
            Ditto Servers
          </PageTitle>

        <div className='grid lg:grid-cols-3 gap-20 mb-40'>
          
            <TeamMember
              name='Ditto.pub'
              avatar='/assets/images/ditto/ditto-logo.png'
              bio='Ditto.pub is the official flagship server of Ditto, running the stable latest version. It is open to anyone, with light moderation. Sign up to get your official Ditto.pub username and join the community!'
              url='https://ditto.pub'
              cta='Sign up'
            />

            <TeamMember
              name='Henhouse.social'
              avatar='/assets/images/ditto/henhouse-logo.png'
              bio='Hen House is a women-only Nostr community built on Ditto. Apply today to connect with the ladies of Nostr! '
              url='https://henhouse.social'
              cta='Sign up'
            />

            <TeamMember
              name='Cobrafuma.com'
              avatar='/assets/images/ditto/cobrafuma.png'
              bio='Cobrafuma é uma comunidade para brasileiros no Nostr. Com o Cobrafuma você pode fazer parte do protocolo Nostr sem medo de censura.              '
              url='https://cobrafuma.com'
              cta='Juntar'
            />

            <TeamMember
              name='Gleasonator.dev'
              avatar='/assets/images/ditto/gleasonator-logo.png'
              bio='Gleasonator is the primary development server for Soapbox and Ditto. Join it to test out all the latest features before they are launched to the public!'
              url='https://gleasonator.dev'
              cta='Sign up'
            />
          </div>

          <PageTitle className='my-16'>
            Soapbox on the Fediverse
          </PageTitle>

          <div className='grid md:grid-cols-3 gap-20 mb-32'>
            {servers.map(server => (
              <div key={server.domain} className='space-y-3'>
                <div>
                  <div className='text-xl'>{server.title}</div>
                  <div><span className='font-bold'><FormattedNumber number={server.userCount} /></span> users</div>
                </div>

                <div>{server.description}</div>

                <a
                  href={`https://${server.domain}`}
                  className='flex space-x-1 items-center text-azure font-bold'
                  target='_blank'
                >
                  <span>Join</span>
                  <IconExternalLink height={20} />
                </a>
              </div>
            ))}
          </div>
        </div>
      </Container>
    </Layout>
  );
}

export const getStaticProps = async () => {
  const servers = await fetchServers();

  return {
    props: { servers },
  };
};