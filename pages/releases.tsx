import { Gitlab, Types as GitlabTypes } from '@gitbeaker/node';

import Container from '../components/container';
import Layout from '../components/layout';
import Release from '../components/release';
import UnifiedMeta from '../components/unified-meta';
import Wrapper from '../components/wrapper';
import markdownToHtml from '../lib/markdownToHtml';

interface Props {
  releases: GitlabTypes.ReleaseSchema[]
}

export default function ReleasesPage({ releases }: Props) {
  return (
    <Layout>
      <UnifiedMeta
        title='Releases | Soapbox'
        description='A list of releases in Soapbox.'
      />
      <Container>
        <Wrapper className='my-12 mb-36 space-y-24'>
          {releases.map((release, i) => (
            <Release
              key={release.tag_name}
              release={release}
              tada={i === 0}
            />
          ))}
        </Wrapper>
      </Container>
    </Layout>
  );
}

export const getStaticProps = async () => {
  const api = new Gitlab({
    host: 'https://gitlab.com',
  });

  const releases = (await api.Releases.all('17765635'))
    .map(release => {
      release.description_html = markdownToHtml(release.description);
      return release;
    });

  return {
    props: { releases },
  };
};
