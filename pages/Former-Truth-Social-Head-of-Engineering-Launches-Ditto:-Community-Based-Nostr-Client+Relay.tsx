import Link from 'next/link';

import Container from '../components/container';
import Layout from '../components/layout';
import PageTitle from '../components/page-title';
import UnifiedMeta from '../components/unified-meta';



export default function DittoLaunchPRPage() {
  return (
    <Layout>
      <UnifiedMeta
        title='Former Truth Social Head of Engineering Launches Ditto: Community-Based Nostr Client + Relay | Soapbox'
      />
      <Container>
        <div className='w-3/4 flex flex-col justify-center mx-auto'>
          <img src='/assets/blog/announcing-ditto.jpeg' alt='Announcing Ditto' className='my-8' />
          <div className='prose my-8 mx-auto mb-16'>
            <h3><strong>FOR IMMEDIATE RELEASE</strong></h3>
            <PageTitle className='my-4'>
              Former Truth Social Head of Engineering Launches Ditto: Community-Based Nostr Client + Relay
            </PageTitle>
            <p><strong>SILICON VALLEY, June 14, 2024 –</strong> Alex Gleason, former Head of Engineering at Truth Social and founder of Soapbox, is proud to <Link className='text-azure underline' href='/blog/announcing-ditto/'>announce the launch of Ditto</Link>, a groundbreaking platform designed to foster freedom-focused communities on Nostr. Ditto seamlessly integrates the best features of Mastodon with the censorship resistance of Nostr, offering a fully customizable and self-hosting solution for decentralized social networking.</p>
            <h2>Reimagining Decentralized Social Media</h2>
            <p><Link className='text-azure underline' href='/ditto'>Ditto</Link> is a self-hosted Nostr client with a built-in relay and a web UI. It offers users robust moderation tools, spam filters, NIP-05 self-service, quote posts, emoji reactions, and zaps — micro transactions on the Bitcoin Lightning Network. Ditto fully implements Mastodon’s REST API, allowing Nostr users to take advantage of over 50 existing Mastodon apps.</p>
            <p>“After a year of dedicated work, I am thrilled to release Ditto,” said Alex Gleason. “This project reflects our commitment to building freedom-loving, user-friendly, and customizable communities on the decentralized web. Ditto brings a new level of functionality and accessibility to Nostr users.”</p>
            <h2>Key Features of Ditto</h2>
            <ul>
              <li><strong>Self-Hosting and Customization:</strong> Ditto empowers users to self-host servers, allowing for the creation of curated communities with customizable themes and logos.</li>
              <li><strong>Robust Moderation and Spam Filters:</strong> Featuring comprehensive moderation tools and a policy plugin system for custom spam filters, Ditto empowers users to build the community they want to see, without censorship.</li>
              <li><strong>NIP-05 Username Service:</strong> Users can request custom usernames, and enjoy a local feed of all members of their server’s domain.</li>
              <li><strong>Trending and Search Features:</strong> Ditto includes trends analysis and full-text search, enhancing discoverability and user engagement.</li>
              <li><strong>Bitcoin Zaps:</strong> Ditto integrates zaps (small Bitcoin payments over the Lightning Network) for monetizing user interactions and community hosting.</li>
              <li><strong>Integration with Mastodon Apps:</strong> Ditto implements the full Mastodon API, unlocking access to any existing Mastodon apps for Nostr users.</li>
            </ul>
            <h2>Flagship Communities and Creator Support</h2>
            <p>Ditto launches with three flagship servers for users to try – <a className='text-azure underline' href='https://ditto.pub/'>Ditto.pub</a>, <a className='text-azure underline' href='http://henhouse.social/'>Henhouse.social</a> (women-only), and <a className='text-azure underline' href='https://gleasonator.dev/'>Gleasonator.dev</a>. Additionally, the Ditto Creator Incubator offers emerging creators resources and support to build and monetize their social communities on Nostr, free from traditional social media constraints.</p>
            <h2>Legacy and Innovation</h2>
            <p>Alex Gleason is <a className='text-azure underline' href='https://www.prnewswire.com/news-releases/truth-social-head-of-engineering-leaves-for-jack-dorsey-backed-alternative-nostr-301879079.html'>known for</a> his development of Soapbox, a polished UI for the Fediverse enjoyed by millions of users worldwide. In 2022, Soapbox became the web frontend for Truth Social, President Donald Trump’s social media platform. In 2023, Gleason launched the <a className='text-azure underline' href='https://mostr.pub/'>Mostr Bridge</a>, a tool to bridge the Fediverse and Nostr used daily by nearly 20,000 users. Soapbox is supported by a grant from Open Sats.</p>
            <p><strong>Press Contact Information</strong></p>
            <p>Contact: Alex Gleason<br />
            Email: contact@soapbox.pub<br />
            Learn more: <a className='text-azure underline' href='https://soapbox.pub/ditto'>https://soapbox.pub/ditto</a><br />
            Press Images: <a className='text-azure underline' href='https://drive.google.com/drive/folders/1ihi50UWsSSKDNTsFC09C8IMuLJlFuDgs?usp=sharing'>Here</a></p>
          </div>
        </div>
      </Container>
    </Layout>
  );
}
