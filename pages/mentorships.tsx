import Banner from '../components/banner';
import Container from '../components/container';
import Layout from '../components/layout';
import MentorshipForm from '../components/mentorship-form';
import PageTitle from '../components/page-title';
import TeamMember from '../components/team-member';
import UnifiedMeta from '../components/unified-meta';
import Wrapper from '../components/wrapper';


export default function MentorshipsPage() {
  return (
    <Layout>
      <UnifiedMeta
        title='Mentorships | Soapbox'
      />
      <Banner 
        heading='Hands-on Experience'
        subheadings={[
          'Learn Nostr and Bitcoin. Get paid.',
        ]}
        mobileFadeClassName='from-purple via-purple'
        backgroundColorClassName='bg-purple'
        imageUrl='/assets/images/mentee.jpg'
        mobileImageUrl='/assets/images/mentee.jpg'
      />
      <Container>
        <Wrapper>

          <PageTitle className='my-16'>
            Mentorship Program
          </PageTitle>

          <div className='space-y-6 pb-36'>
            <h2 className='text-2xl font-semibold leading-snug'>
              Soapbox mentorships for new developers
            </h2>
            <p>
             Soapbox provides mentorship opportunities for new developers to contribute to our open source projects focused on building decentralized social media on Nostr. We prioritize individuals in developing countries who have limited access to career opportunities, offering them a monthly stipend in Bitcoin to foster economic empowerment and financial freedom.
            </p>

            <h3 className='text-xl font-semibold leading-snug'>
              Mentorships include:
            </h3>
            <ul className='list-disc pl-8'>
              <li>
                Monthly stipend paid in Bitcoin
              </li>
              <li>
                1:1 mentorship and code review
              </li>
              <li>
                Become part of a passionate global team of developers
              </li>
              <li>
                Get credit for your work, and grow your portfolio
              </li>
            </ul>

            <p className='text-sm'>Don't see what you're looking to work on? Browse <a className='text-azure underline' href='https://gitlab.com/soapbox-pub/soapbox/-/issues' target='_blank'>open issues on GitLab</a> to begin contributing today and get involved!</p>

            <div className='relative flex py-8 items-center'>
              <div className='flex-grow border-t border-gray-200'></div>
            </div>

          <PageTitle className='my-20'>
            Meet the Mentees
          </PageTitle>

          <div className='pt-8 grid md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 gap-20'>
            <a href='https://ditto.pub/@patrick@patrickdosreis.com' target='_blank'>
              <TeamMember
                name='Patrick (Brazil)'
                avatar='/assets/avatars/patrick.png'
                url=''
                bio=''
                cta=''
              />
            </a>

            <a href='https://ditto.pub/@danidfra@danidfra.com' target='_blank'>
              <TeamMember
                name='Danidfra (Argentina)'
                avatar='/assets/avatars/danidfra.jpeg'
                bio=''
                url=''
                cta=''
              />
            </a>

            <a href='https://ditto.pub/@shan@shantaram.xyz' target='_blank'>
              <TeamMember
                name='shantaram (India)'
                avatar='/assets/avatars/shantaram.jpeg'
                bio=''
                url=''
                cta=''
              />
            </a>

          </div>

          <p className='text-sm text-right'>Meet the whole team <a className='text-azure underline' href='https://soapbox.pub/about/' target='_blank'>here →</a></p>

          <div className='relative flex py-8 items-center'>
            <div className='flex-grow border-t border-gray-200'></div>
          </div>

            <MentorshipForm />
          </div>
        
        </Wrapper>

      </Container>
    </Layout>
  );
}
