# soapbox.pub

The official website for Soapbox.

[https://soapbox.pub](https://soapbox.pub/)

It contains the landing page, blog, install guide, and other useful pages.
Its purpose is to serve as a hub for all things Soapbox.
It should make Soapbox look appealing, and help people find what they're looking for.

## Technology stack

This website is built with React, TypeScript, and Next.js, then exported to static HTML, CSS, and JS.
React is a client-side framework, but Next.js is a hybrid approach which allows us to build pages in React that ultimately don't require JavaScript in the user's browser.

- [React](https://reactjs.org/) to design the pages.
- [Next.js](https://nextjs.org/) to export the code into HTML/CSS/JS.
- [Tailwind](https://tailwindcss.com/) as a utility CSS framework.
- [Contentlayer](https://www.contentlayer.dev/) to connect Next.js with our local Markdown files.
- [MDX](https://mdxjs.com/) for rich content in blog posts and other pages.

A neat thing about Next.js is that it can retrieve external data from anywhere to display on its pages.
For example, it can use the GitLab API to display information about repo activity, or scrape Fediverse instances.

## Content pulled from other sources

Next.js allows us to easily pull data from external sources at build time. This makes the site more dynamic, as the site content can get updated between builds without code changes.

### Top contributors

The top contributors on the homepage are pulled from GitLab.
Four contributors are pulled from the past 300 commits.
The algorithm is in [`lib/contributors.ts`](https://gitlab.com/soapbox-pub/soapbox.pub/-/blob/main/lib/contributors.ts). You must be a member of the Soapbox GitLab group to show up.

### About page

A similar list of contributors is displayed on the About page.
This one is ranked by all-time contributions instead of recent contributions.
The top 6 (besides maintainers) are displayed.

## Local development

You will need to install Node.js and [Yarn](https://yarnpkg.com/) (hint: `npm install -g yarn`), then:

```sh
# Do this once to install dependencies
yarn

# Run the local development environment
yarn dev
```

## Deployment

This site is deployed automatically.

When code is merged into the `main` branch, GitLab CI builds a new version of the site and deploys it to GitLab Pages.

## License

© Alex Gleason & other soapbox.pub contributors  

soapbox.pub is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

soapbox.pub is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with soapbox.pub. If not, see <https://www.gnu.org/licenses/>.