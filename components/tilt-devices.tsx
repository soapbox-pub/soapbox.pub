import Tilt from 'react-parallax-tilt';

const TiltDevices: React.FC = () => {
  return (
    <Tilt
      tiltMaxAngleX={3}
      tiltMaxAngleY={3}
      style={{ transformStyle: 'preserve-3d' }}
    >
      <div
        className='relative'
        style={{ transformStyle: 'preserve-3d' }}
      >
        <img
          className='mx-auto'
          src='/assets/soapbox-render-tablet2.png'
          alt='Soapbox'
          width={1200}
          height={747}
        />

        <img
          className='hidden xl:block absolute inset-0 mx-auto'
          style={{ transform: 'translateX(calc(50% + 80px)) translateY(calc(50% - 30px)) translateZ(60px)' }}
          src='/assets/soapbox-render-phone2.png'
          alt='Soapbox'
          width={550}
          height={426}
        />
      </div>
    </Tilt>
  );
};

export default TiltDevices;