import Head from 'next/head';

interface IUnifiedMeta {
  title?: string;
  description?: string;
  image?: string;
}

/** Duplicates meta tags for OpenGraph, Twitter, and standard. */
const UnifiedMeta: React.FC<IUnifiedMeta> = ({ title, description, image }) => {
  return (
    <Head>
      {title && <title>{title}</title>}
      {title && <meta property='og:title' content={title} key='og:title' />}
      {title && <meta name='twitter:title' content={title} key='twitter:title' />}
      {description && <meta name='description' content={description} key='description' />}
      {description && <meta property='og:description' content={description} key='og:description' />}
      {description && <meta name='twitter:description' content={description} key='twitter:description' />}
      {image && <meta property='og:image' content={image} key='og:image' />}
      {image && <meta name='twitter:image' content={image} key='twitter:image' />}
      {image && <meta name='image' content={image} key='image' />}
    </Head>
  );
};

export default UnifiedMeta;
