import { IconArrowRight } from '@tabler/icons';
import clsx from 'clsx';
import React, { useState, useEffect } from 'react';

import Button from './button';

interface BannerProps {
  backgroundColorClassName?: string;
  heading: string;
  subheadings: string[];
  paragraph?: string;
  imageUrl: string;
  ctaText?: string;
  ctaUrl?: string;
}

const SimpleBanner: React.FC<BannerProps> = ({ heading, subheadings, paragraph, imageUrl, ctaText, ctaUrl, backgroundColorClassName }) => {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 800);
    };

    handleResize();
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div className={clsx(
      'flex flex-col-reverse items-center space-y-8 lg:flex-row lg:space-x-8 px-8 md:px-16 lg:pl-32 py-8', 
      backgroundColorClassName,
    )}>
      <div className='flex-1 text-center lg:text-left md:w-2/3 lg:w-1/2 max-w-2xl space-y-8  mx-auto px-5'>
        <h1 className='text-white text-7xl lg:text-7xl font-bold leading-tight'>{heading}</h1>
        {subheadings.map((subheading, index) => (
          <h2 key={index} className='text-white text-lg md:text-xl lg:text-3xl 2xl:text-4xl font-semibold'>{subheading}</h2>
        ))}
        {paragraph && <p className='text-white text-lg mb-4'>{paragraph}</p>}
        {ctaUrl && ctaText && (
          <Button theme='secondary' href={ctaUrl} group>
            <span className='flex items-center space-x-2 group'>
              <span>{ctaText}</span>
              <IconArrowRight className='transition-all w-5 group-hover:translate-x-1' />
            </span>
          </Button>
        )}
      </div>
      <img src={imageUrl} alt={heading} className='lg:w-1/2' />
    </div>
  );
};

export default SimpleBanner;
