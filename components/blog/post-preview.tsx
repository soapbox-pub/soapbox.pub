import Link from 'next/link';

import DateFormatter from '../date-formatter';

import CoverImage from './cover-image';

import type { Author } from 'contentlayer/generated';

type Props = {
  title: string
  coverImage?: string
  date: string
  excerpt: string
  author?: Author
  slug: string
}

const PostPreview = ({
  title,
  coverImage,
  date,
  excerpt,
  slug,
}: Props) => {
  return (
    <div>
      {coverImage && (
        <div className='mb-5'>
          <CoverImage slug={slug} title={title} src={coverImage} />
        </div>
      )}
      <h3 className='text-3xl mb-3 leading-snug'>
        <Link
          as={`/blog/${slug}`}
          href='/blog/[slug]'
          className='hover:underline'
        >
          {title}
        </Link>
      </h3>
      <div className='mb-4 text-gray-500'>
        <DateFormatter dateString={date} />
      </div>
      <p className='text-gray-800 mb-4'>
        {excerpt.slice(0, 150)}&hellip;
      </p>
    </div>
  );
};

export default PostPreview;
