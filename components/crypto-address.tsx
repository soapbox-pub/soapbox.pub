import { IconExternalLink } from '@tabler/icons';

import { getExplorerUrl } from '../lib/crypto/block-explorer';
import { getTitle } from '../lib/crypto/coin-db';

import CryptoIcon from './crypto-icon';
import CopyableInput from './ui/copyable-input';
import HStack from './ui/hstack';
import Stack from './ui/stack';
import Text from './ui/text';

export interface ICryptoAddress {
  address: string
  ticker: string
  note?: string
}

const CryptoAddress: React.FC<ICryptoAddress> = ({ address, ticker, note }) => {
  const title = getTitle(ticker);
  const explorerUrl = getExplorerUrl(ticker, address);

  return (
    <Stack>
      <HStack alignItems='center' className='mb-1'>
        <CryptoIcon
          className='flex items-start justify-center w-6 mr-2.5 rtl:ml-2.5 rtl:mr-0'
          ticker={ticker}
          title={title}
        />

        <Text weight='bold'>{title || ticker.toUpperCase()}</Text>

        <HStack alignItems='center' className='ml-auto'>
          {/* <a className='text-gray-500 ml-1 rtl:ml-0 rtl:mr-1' href='#' onClick={handleModalClick}>
            <Icon src={require('@tabler/icons/qrcode.svg')} size={20} />
          </a> */}

          {explorerUrl && (
            <a className='text-gray-500 ml-1 rtl:ml-0 rtl:mr-1' href={explorerUrl} target='_blank'>
              <IconExternalLink width={20} />
            </a>
          )}
        </HStack>
      </HStack>

      {note && (
        <Text>{note}</Text>
      )}

      <CopyableInput value={address} />
    </Stack>
  );
};

export default CryptoAddress;
