import { getLastModified } from './git';

interface FileInfo {
  lastModified: string
  sourceUrl: string
}

const getFileInfo = (filename: string): FileInfo => {
  return {
    lastModified: getLastModified(filename),
    sourceUrl: `https://gitlab.com/soapbox-pub/soapbox.pub/-/blob/main/${filename}`,
  };
};

export {
  getFileInfo,
};