# Ditto and Nostr

Ditto is an application that allows you to connect and interact with the decentralized Nostr protocol. One of the key advantages of using Ditto is that it supports a decentralized experience. This means that you can access your Ditto Nostr account from any other Nostr app, including well-known clients such as Damus on iOS, Primal, or Amethyst.

Your Nostr account data is not locked to any one app. Instead, you have the freedom to choose the client that best fits your needs while maintaining the same user account. The flexibility and accessibility provided by Nostr's decentralized nature ensures a seamless experience across various platforms.

Whether you're on your mobile device or desktop, logging into your Ditto account is as easy as using any other Nostr application. Enjoy the variety of features and interfaces each app offers while staying connected to your Nostr community!